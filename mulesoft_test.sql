CREATE DATABASE  IF NOT EXISTS `mulesoft_test`;
USE `mulesoft_test`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fName` varchar(45) NOT NULL,
  `lName` varchar(45) NOT NULL,
  `age` int(2) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO mulesoft_test.users (`fName`, `lName`, `age`, `phone`, `email`) VALUES ('vip', 'naja', 27, '0899999999', 'vip@gmail.com');
INSERT INTO mulesoft_test.users (`fName`, `lName`, `age`, `phone`, `email`) VALUES ('test', 'java', 26, '0866666666', 'vip@gmail.com');